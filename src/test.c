#include "test.h"
#include "util.h"

#define BLOCK_SIZE_INST 4096
#define BLOCK_SIZE_INST_ONE 1

#define QUERY_SIZE_VSM 1024
#define QUERY_SIZE_SM 2048
#define QUERY_SIZE_MED 8192
#define QUERY_SIZE_LAR 16384

static void end_test(void* heap, int query, const char* const testName) {
    if (munmap(heap, size_from_capacity((block_capacity) {.bytes = query}).bytes) != 0){
        perror("Ошибка munmap!");
    }
    fprintf(stdout, "%s%s", testName, " is successful!\n");
}

static void check_heap(const void* const heap, const char* const msg) {
    printf("%s: %s", msg, "\n");
    debug_heap(stdout, heap);
}

void test_1(void) {
    void* heap = heap_init(BLOCK_SIZE_INST);
    if (!heap) {
        printf("Test number 1 failed, heap is not initialized.");
        return;
    }
    check_heap(heap, "Heap after init");

    void* mem = _malloc(QUERY_SIZE_SM);
    if (!mem) {
        printf("Test number 1 failed, memory is not allocated.");
        return;
    }
    check_heap(heap, "Heap after alloc");

    _free(mem);
    check_heap(heap, "Heap after free mem");

    end_test(heap, BLOCK_SIZE_INST, "Test number 1");
}

void test_2(void) {
    void* heap = heap_init(BLOCK_SIZE_INST);
    if (!heap) {
        printf("Test number 2 failed, heap is not initialized.");
        return;
    }
    check_heap(heap, "Heap after init");

    void* mem1 = _malloc(QUERY_SIZE_VSM);
    void* mem2 = _malloc(QUERY_SIZE_VSM);
    if (!mem1 || !mem2){
        printf("Test number 2 failed, memory is not allocated.");
        return;
    }
    check_heap(heap, "Heap after alloc");

    _free(mem1);
    if (!mem2) {
        printf("Test number 2 failed, release of the first damaged the second.");
        return;
    }
    check_heap(heap, "Heap after free mem");

    _free(mem2);
    end_test(heap, BLOCK_SIZE_INST, "Test number 2");
}

void test_3(void) {
    void* heap = heap_init(BLOCK_SIZE_INST);
    if (!heap) {
        printf("Test number 3 failed, heap is not initialized.");
        return;
    }
    check_heap(heap, "Heap after init");

    void* mem1 = _malloc(QUERY_SIZE_VSM);
    void* mem2 = _malloc(QUERY_SIZE_VSM);
    void* mem3 = _malloc(QUERY_SIZE_VSM);
    if (!mem1 || !mem2 || !mem3) {
        printf("Test number 3 failed, memory is not allocated.");
        return;
    }
    check_heap(heap, "Heap after alloc");

    _free(mem1);
    _free(mem3);
    if (!mem2){
        printf("Test number 3 failed, release of the first damaged the second.");
        return;
    }
    check_heap(heap, "Heap after free mem");

    end_test(heap, BLOCK_SIZE_INST, "Test number 3");
}

void test_4(void) {
    void* heap = heap_init(BLOCK_SIZE_INST_ONE);
    if (!heap) {
        printf("Test number 4 failed, heap is not initialized.");
        return;
    }
    check_heap(heap, "Heap after init");

    void* mem1 = _malloc(QUERY_SIZE_MED);
    void* mem2 = _malloc(QUERY_SIZE_LAR);
    if (!mem1 || !mem2) printf("Test number 4 failed, memory is not allocated.");
    check_heap(heap, "Heap after alloc");

    struct block_header* header1 = block_get_header(mem1);
    struct block_header* header2 = block_get_header(mem2);

    if (header1 && header2) {
        int total_capacity = (int) (header1->capacity.bytes + header2->capacity.bytes +
                                    ((struct block_header* ) heap)->capacity.bytes);
        end_test(heap,
                 total_capacity,
                 "Test number 4");
    } else {
        printf("Test number 4 failed, header1 or header2 is null.");
        return;
    }

    if (!header1 || header1->next != header2) {
        printf("Test number 4 failed, headers are not linked.");
        return;
    }


}

void test_5(void) {
    void* heap = heap_init(BLOCK_SIZE_INST_ONE);
    if (!heap) {
        printf("Test number 5 failed, heap is not initialized.");
        return;
    }
    check_heap(heap, "Heap after init");

    void* mem1 = _malloc(QUERY_SIZE_MED);
    struct block_header* header1 = block_get_header(mem1);
    if (!mem1) {
        printf("Test number 5 failed, the first memory is not allocated.");
        return;
    }
    if (!header1) {
        printf("Test number 5 failed, header is not find.");
        return;
    }
    check_heap(heap, "Heap after alloc.");

    void* newRegion = mmap((*header1).contents + (*header1).capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
                           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    void* mem2 = _malloc(QUERY_SIZE_MED);
    struct block_header* header2 = block_get_header(mem2);
    if (!mem2) {
        printf("Test number 5 failed, the second memory is not allocated");
        return;
    }
    check_heap(heap, "Heap after second malloc");

    _free(mem1);
    _free(mem2);
    check_heap(heap, "Heap after realising");

    munmap(newRegion, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    end_test(heap,
             (int) (header1->capacity.bytes + header2->capacity.bytes +
                    ((struct block_header* ) heap)->capacity.bytes),
             "Test number 5");
}
