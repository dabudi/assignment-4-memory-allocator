#ifndef TEST_H
#define TEST_H

#include "mem.h"
#include "mem_internals.h"

#ifndef MAP_ANONYMOUS
#define MAP_ANONYMOUS 0x20
#endif

#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x100000
#endif

void test_1(void);
void test_2(void);
void test_3(void);
void test_4(void);
void test_5(void);

#endif
