#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t pages_count ( size_t mem ) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages ( size_t mem ) { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}


static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap((void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static struct region alloc_region(void const* addr, size_t query) {
    size_t const full_size = size_from_capacity(((block_capacity){query})).bytes;
    size_t const region_size = region_actual_size(full_size);

    void* malloc_exec_addr = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

    if (malloc_exec_addr == MAP_FAILED) {
        malloc_exec_addr = map_pages(addr, region_size, 0);
        if (malloc_exec_addr != MAP_FAILED) {
            block_init(malloc_exec_addr, (block_size) {region_size}, NULL);
            return (struct region) {.addr = malloc_exec_addr, .size = region_size, .extends = false};
        } else {
            return REGION_INVALID;
        }
    }

    block_init(malloc_exec_addr, (block_size) {region_size}, NULL);
    return (struct region) {.addr = malloc_exec_addr, .size = region_size, .extends = true};
}

static void* block_after(struct block_header const* block);

void* heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;
    return region.addr;
}

void heap_term() {
    void* start_region = HEAP_START;
    size_t now_region_size = 0;
    struct block_header* now_block = start_region;

    while (now_block) {
        struct block_header* temp_header = now_block->next;

        now_region_size += size_from_capacity(now_block->capacity).bytes;
        if (now_block->next && block_after(now_block) != now_block->next) {
             if (munmap(start_region, now_region_size) != 0){
                perror("Ошибка munmap!");
             }
            start_region = temp_header;
            now_region_size = 0;

        } else if (!temp_header) {
            if (munmap(start_region, now_region_size) != 0) {
                perror("Ошибка munmap!");
            }            
            break;
        }
        now_block = temp_header;
    }
}

static bool block_splittable(struct block_header* restrict block, size_t query) {
    return block->is_free && query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header* block, size_t query) {
    if (!block_splittable(block, query)) return false;

    void* new_block_addr = block->contents + query;

    block_size const new_block_size = (block_size){block->capacity.bytes - query};
    block_init(new_block_addr, new_block_size, block->next);
    block->capacity.bytes = query;
    block->next = new_block_addr;
    return true;
}

static void* block_after(struct block_header const* block) {
    return (void*)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const* fst,
        struct block_header const* snd) {
    return (void* ) snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header* block) {
    if (!block->next) return false;

    if (!mergeable(block, block->next)) return false;

    size_t const capacity = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
    block->capacity = (block_capacity){capacity};
    block->next = block->next->next;
    return true;
}

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last(struct block_header* restrict block, size_t sz) {
    if (!block) return (struct block_search_result){BSR_CORRUPTED, block};
    do {
        if (!block->is_free) {
            if (block->next)
                block = block->next;
            else
                break;
            continue;
        };

        bool flag = true;
        while (flag) {
            flag = try_merge_with_next(block);
        }
        if (block_is_big_enough(sz, block))
            return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
        if (block->next)
            block = block->next;
        else
            break;
    } while (block);
    return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
}

static struct block_search_result try_memalloc_existing(size_t query, struct block_header* block) {
    if(query < BLOCK_MIN_CAPACITY){
        return (struct block_search_result) {.type = BSR_CORRUPTED};
    }
    struct block_search_result const result = find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = !true;
    }
    return result;
}

static struct block_header* grow_heap(struct block_header* restrict last, size_t query) {
    if (!last) return NULL;
    struct region const new_alloc_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&new_alloc_region)) return NULL;
    if (!new_alloc_region.extends || !last->is_free) {
        last->next = new_alloc_region.addr;
        return new_alloc_region.addr;
    }
    last->capacity.bytes += new_alloc_region.size;
    return last;
}

static struct block_header* memalloc(size_t query, struct block_header* heap_start) {
    if (!heap_start) return NULL;
    query = size_max(query, BLOCK_MIN_CAPACITY);
    struct block_search_result result = try_memalloc_existing(query, heap_start);
    switch(result.type)
    {
        case BSR_FOUND_GOOD_BLOCK:
            return result.block;
        case BSR_CORRUPTED:
            return NULL;
        case BSR_REACHED_END_NOT_FOUND:
            result.block = grow_heap(result.block, query);
            result = try_memalloc_existing(query, result.block);
            if (result.type == BSR_FOUND_GOOD_BLOCK) {
                return result.block;
            }
            return NULL;
        default:
            return NULL;
    }   
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;

  return NULL;
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free(void* mem) {
    if (!mem) return;
    struct block_header* header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header));
}
